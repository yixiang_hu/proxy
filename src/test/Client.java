package test;

import proxy.Proxy;

/**
 * @Author 胡先生
 * 2019/11/19-10:59
 */
public class Client {

    public static void main(String[] args) {
        //客户直接找中介
        Proxy p=new Proxy();
        //客户进行方法的调用
        p.rent();


    }

}
