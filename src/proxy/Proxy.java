package proxy;

/**
 * @Author 胡先生
 * 2019/11/19-10:51
 */
//代理者，只有实现了租房的接口，才能证明他的身份是房产中介
public class Proxy implements RentHouse {

    //中介先找到房东
    private  Landloard l=new Landloard();

    @Override
    public void rent() {
        publicInfo();
        getPhone();
        lookHouse();
        //本质工作,调用房东的租房方法
        l.rent();
        getFee();

    }

    private void getFee() {
        System.out.println("房子租出去，我收到了中介费，开心~~");
    }

    private void lookHouse() {
        System.out.println("我是中介，我带客户去看房了");
    }

    private void getPhone() {
        System.out.println("我是中介，我接了100个电话");
    }

    private void publicInfo() {
        System.out.println("我是中介，我发布了租房信息");
    }
}
