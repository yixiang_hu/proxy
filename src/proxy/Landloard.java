package proxy;

/**
 * @Author 胡先生
 * 10:49
 */
//房东，真实有效地出租房子的方法
public class Landloard implements RentHouse{

    @Override
    public void rent() {

        System.out.println("我是房东，我把房子租出去了");

    }

}
